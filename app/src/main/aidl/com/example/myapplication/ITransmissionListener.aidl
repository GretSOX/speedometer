// ITransmissionListener.aidl
package com.example.myapplication;

// Declare any non-default types here with import statements

interface ITransmissionListener {
    oneway void updateRpm(float thousandRpm);
    oneway void updateSpeed(float speed);
}
