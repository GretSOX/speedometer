// ITransmissionService.aidl
package com.example.myapplication;

import com.example.myapplication.ITransmissionListener;

// Declare any non-default types here with import statements

interface ITransmissionService {

    void subscribeToSpeed(in ITransmissionListener listener);
}
