package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter

class SpeedometerAdapter : PagerAdapter() {
    private var tachometerView: SpeedometerView? = null
    private var speedometerView: SpeedometerView? = null

    var rpm: Float = 0f
        set(value) {
            field = value
            tachometerView?.setValue(value)
        }

    var speed: Float = 0f
        set(value) {
            field = value
            speedometerView?.setValue(value)
        }

    override fun isViewFromObject(view: View, key: Any): Boolean = key == view

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        return if (position == 0) {
            (LayoutInflater
                .from(container.context)
                .inflate(R.layout.item_tachometer, container, false) as SpeedometerView)
                .also {
                    container.addView(it)
                    it.setValue(rpm)
                    tachometerView = it
                }

        } else {
            (LayoutInflater
                .from(container.context)
                .inflate(R.layout.item_speedometer, container, false) as SpeedometerView)
                .also {
                    container.addView(it)
                    it.setValue(speed)
                    speedometerView = it
                }
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, key: Any) {
        container.removeView(key as View)
        if (position == 0) {
            tachometerView = null
        } else {
            speedometerView = null
        }
    }

    override fun getCount(): Int = 2

}