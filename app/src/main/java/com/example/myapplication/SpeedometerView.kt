package com.example.myapplication

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.annotation.ColorInt
import androidx.annotation.Dimension
import androidx.annotation.Size
import kotlin.math.*

class SpeedometerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : View(context, attrs, defStyleAttr, defStyleRes) {

    private val titledSerifRate: Int
    private val intermediateSerifRate: Int
    private val serifStep: Float

    @ColorInt
    private val titledSerifColor: Int
    @ColorInt
    private val intermediateSerifColor: Int
    @ColorInt
    private val serifColor: Int

    @Dimension
    private val titledSerifLength: Float
    @Dimension
    private val intermediateSerifLength: Float
    @Dimension
    private val serifLength: Float

    @Dimension
    private val titledSerifWidth: Float
    @Dimension
    private val intermediateSerifWidth: Float
    @Dimension
    private val serifWidth: Float

    @Dimension
    private val titleSerifPadding: Float

    private val startDegree: Int
    private val sweepDegree: Int

    private val minValue: Int
    private val maxValue: Int

    private val textSize: Float
    @ColorInt
    private val textColor: Int

    @ColorInt
    private val pointerColor: Int
    private val pointerWidth: Float

    private var deialRadius: Float = 0F
    private var centerX: Float = 0F
    private var centerY: Float = 0F

    private var value: Float = 0F

    private val interpolator = DecelerateInterpolator(1f)

    private val animatorListener = ValueAnimator.AnimatorUpdateListener { animator ->
        value = animator.animatedValue as Float
        invalidate()
    }

    private var animator: ValueAnimator =
        ValueAnimator
            .ofFloat(0F, 1F)
            .setDuration(200)
            .apply {
                setInterpolator(this@SpeedometerView.interpolator)
                addUpdateListener(animatorListener)
            }

    private val pointerPaint = Paint().apply {
        style = Paint.Style.STROKE
        isAntiAlias = true
    }

    private var markUpBitmap: Bitmap? = null

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.SpeedometerView,
            0, defStyleRes
        ).apply {
            try {
                minValue = getInt(R.styleable.SpeedometerView_minValue, 0)
                maxValue = getInt(R.styleable.SpeedometerView_maxValue, 180)

                titledSerifRate = getInt(R.styleable.SpeedometerView_titledSerifRate, 10)
                intermediateSerifRate = getInt(R.styleable.SpeedometerView_intermediateSerifRate, 5)
                serifStep = getFloat(R.styleable.SpeedometerView_serifStep, 2F)

                titledSerifLength =
                    getDimension(R.styleable.SpeedometerView_titledSerifLength, dpToPx(8F))
                intermediateSerifLength =
                    getDimension(R.styleable.SpeedometerView_intermediateSerifLength, dpToPx(6F))
                serifLength = getDimension(R.styleable.SpeedometerView_serifLength, dpToPx(4F))

                titledSerifWidth =
                    getDimension(R.styleable.SpeedometerView_titledSerifWidth, dpToPx(3F))
                intermediateSerifWidth =
                    getDimension(R.styleable.SpeedometerView_intermediateSerifWidth, dpToPx(2F))
                serifWidth = getDimension(R.styleable.SpeedometerView_serifWidth, dpToPx(1F))

                titleSerifPadding =
                    getDimension(R.styleable.SpeedometerView_titleSerifPadding, dpToPx(4F))

                startDegree = getInt(R.styleable.SpeedometerView_startDegree, -210)
                sweepDegree = getInt(R.styleable.SpeedometerView_sweepDegree, 240)

                titledSerifColor =
                    getColor(R.styleable.SpeedometerView_titledSerifColor, Color.BLACK)
                intermediateSerifColor =
                    getColor(R.styleable.SpeedometerView_intermediateSerifColor, Color.BLACK)
                serifColor = getColor(R.styleable.SpeedometerView_serifColor, Color.GRAY)

                pointerColor = getColor(R.styleable.SpeedometerView_pointerColor, Color.RED)
                pointerWidth = getDimension(R.styleable.SpeedometerView_painterWidth, dpToPx(3f))
                pointerPaint.apply {
                    color = pointerColor
                    strokeWidth = pointerWidth
                }
            } finally {
                recycle()
            }
        }

        context.theme.obtainStyledAttributes(
            attrs,
            intArrayOf(
                android.R.attr.textSize,
                android.R.attr.textColor
            ),
            0,
            defStyleRes
        ).apply {
            try {
                textSize = getDimension(0, spToPx(16f))
                textColor = getColor(1, Color.BLACK)
            } finally {
                recycle()
            }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        val widthWithoutPadding = w.toFloat() - paddingLeft - paddingEnd
        val heightWithoutPadding = h.toFloat() - paddingTop - paddingBottom
        centerX = paddingLeft + widthWithoutPadding / 2
        centerY = paddingTop + heightWithoutPadding / 2
        deialRadius = min(widthWithoutPadding / 2, heightWithoutPadding / 2)
        markUpBitmap = if (w > 0 && h > 0) generateBackgroundBitmap(w, h) else null
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        markUpBitmap?.also { canvas.drawBitmap(it, 0f, 0f, null) }
        val pointerDegree =
            value
                .restrict(minValue.toFloat(), maxValue.toFloat())
                .fractionForRange(minValue.toFloat(), maxValue.toFloat())
                .fractionToPositionInRange(
                    startDegree.toFloat(),
                    startDegree.toFloat() + sweepDegree
                )
        val rad = Math.toRadians(pointerDegree.toDouble())
        val cos = cos(rad).toFloat()
        val sin = sin(rad).toFloat()
        canvas.drawLine(
            centerX + cos * deialRadius,
            centerY + sin * deialRadius,
            centerX,
            centerY,
            pointerPaint
        )
    }

    fun setValue(value: Float, animate: Boolean = true) {
        if (animate) {
            animator.apply {
                cancel()
                setFloatValues(this@SpeedometerView.value, value)
                start()
            }
        } else {
            animator.cancel()
            this.value = value
            invalidate()
        }

    }

    private fun generateBackgroundBitmap(w: Int, h: Int): Bitmap {
        val btm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(btm)

        val scaleRange = maxValue - minValue

        val serifAngle = sweepDegree * serifStep / scaleRange.toDouble()

        val serifCount = (scaleRange / serifStep).toInt()

        // serif types  0 - simple serif
        //              1 - intermediate serif
        //              2 - titled serif

        val groupedSerif = (0..serifCount)
            .groupBy { index ->
                val value = index + minValue
                when {

                    value % titledSerifRate == 0 -> 2
                    value % intermediateSerifRate == 0 -> 1
                    else -> 0
                }
            }

        val serifPaint = Paint()
            .apply {
                style = Paint.Style.STROKE
                isAntiAlias = true
            }

        groupedSerif[0] //0 - simple serif
            ?.let { serifs ->
                val resultHolder = FloatArray(serifs.size * 4)
                serifs.forEachIndexed { index, serifIndex ->
                    getCoordsFor(
                        startDegree + serifAngle * serifIndex,
                        serifLength,
                        deialRadius,
                        centerX,
                        centerY,
                        resultHolder,
                        index * 4
                    )
                }
                return@let resultHolder
            }
            ?.also { coords ->
                serifPaint.apply {
                    color = serifColor
                    strokeWidth = serifWidth
                }
                canvas.drawLines(coords, serifPaint)
            }

        groupedSerif[1] //1 - intermediate serif
            ?.let { serifs ->
                val resultHolder = FloatArray(serifs.size * 4)
                serifs.forEachIndexed { index, serifIndex ->
                    getCoordsFor(
                        startDegree + serifAngle * serifIndex,
                        intermediateSerifLength,
                        deialRadius,
                        centerX,
                        centerY,
                        resultHolder,
                        index * 4
                    )
                }
                return@let resultHolder
            }
            ?.also { coords ->
                serifPaint.apply {
                    color = intermediateSerifColor
                    strokeWidth = intermediateSerifWidth
                }
                canvas.drawLines(coords, serifPaint)
            }

        groupedSerif[2] //2 - titled serif
            ?.let { serifs ->
                val resultHolder = FloatArray(serifs.size * 4)
                serifs.forEachIndexed { index, serifIndex ->
                    getCoordsFor(
                        startDegree + serifAngle * serifIndex,
                        titledSerifLength,
                        deialRadius,
                        centerX,
                        centerY,
                        resultHolder,
                        index * 4
                    )
                }
                return@let resultHolder
            }
            ?.also { coords ->
                serifPaint.apply {
                    color = titledSerifColor
                    strokeWidth = titledSerifWidth
                }
                canvas.drawLines(coords, serifPaint)
            }


        groupedSerif[2] //2 - titled serif
            ?.also { titledSerifs ->
                val textPaint =
                    Paint().apply {
                        textSize = this@SpeedometerView.textSize
                        color = textColor
                        isAntiAlias = true
                    }
                val textBounds = Rect()

                titledSerifs.forEach { serifIndex ->
                    val value = minValue + serifStep * serifIndex
                    val rad = Math.toRadians(startDegree + serifAngle * serifIndex)
                    val cos = cos(rad).toFloat()
                    val sin = sin(rad).toFloat()
                    val text = value.toInt().toString()
                    textPaint.getTextBounds(text, 0, text.length, textBounds)

                    val circumscribedCircleRadius =
                        sqrt(
                            textBounds.width().toFloat().pow(2)
                                + textBounds.height().toFloat().pow(2)
                        ) / 2
                    val distanceToCenterCircumscribedCircle =
                        deialRadius - titledSerifLength - titleSerifPadding
                    val x =
                        cos * distanceToCenterCircumscribedCircle + cos * circumscribedCircleRadius * -cos.sign
                    val y =
                        sin * distanceToCenterCircumscribedCircle + sin * circumscribedCircleRadius * sin.sign
                    canvas.drawText(text, centerX + x, centerY + y, textPaint)
                }
            }


        return btm
    }

    private fun getCoordsFor(
        degree: Double,
        length: Float,
        radius: Float,
        centerX: Float,
        centerY: Float,
        @Size(multiple = 4) resultHolder: FloatArray,
        offset: Int
    ) {
        val rad = Math.toRadians(degree)
        val cos = cos(rad).toFloat()
        val sin = sin(rad).toFloat()
        resultHolder[offset] = centerX + cos * radius
        resultHolder[offset + 2] = centerX + cos * (radius - length)
        resultHolder[offset + 1] = centerY + sin * radius
        resultHolder[offset + 3] = centerY + sin * (radius - length)
    }

}