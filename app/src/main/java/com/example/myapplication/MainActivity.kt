package com.example.myapplication

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val mainThreadHandler = Handler()

    private val serviceConnection = object : ServiceConnection {

        private val listener = object : ITransmissionListener.Stub() {

            private val viewPagerAdapter: SpeedometerAdapter?
                get() = viewPager?.adapter as? SpeedometerAdapter

            override fun updateRpm(thousandRpm: Float) {
                mainThreadHandler.post {
                    viewPagerAdapter
                        ?.also { it.rpm = thousandRpm }
                        ?: run { (tachometerView as? SpeedometerView)?.setValue(thousandRpm) }
                }
            }

            override fun updateSpeed(speed: Float) {
                mainThreadHandler.post {
                    viewPagerAdapter
                        ?.also { it.speed = speed }
                        ?: run { (speedometerView as? SpeedometerView)?.setValue(speed) }
                }
            }
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder) {
            val speedGeneratorService = ITransmissionService.Stub.asInterface(service)
            speedGeneratorService.subscribeToSpeed(listener)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            connectToService()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setupImmersiveMode()
        setContentView(R.layout.activity_main)
        viewPager?.apply {
            adapter = SpeedometerAdapter()
        }
        connectToService()
    }

    override fun onDestroy() {
        super.onDestroy()
        mainThreadHandler.removeCallbacksAndMessages(null)
        unbindService(serviceConnection)
    }


    private fun connectToService() {
        val intent = Intent(this, TrasmissionService::class.java)
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

}
