package com.example.myapplication

import android.content.Context
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import android.view.Window
import androidx.annotation.Dimension

private var immersiveFlags: Int = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    or View.SYSTEM_UI_FLAG_FULLSCREEN
    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

fun Window.setupImmersiveMode() {
    decorView.systemUiVisibility = immersiveFlags
    decorView.setOnSystemUiVisibilityChangeListener { visibility ->
        if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
            decorView.systemUiVisibility = immersiveFlags
        }
    }
}

fun dpToPx(displayMetrics: DisplayMetrics, @Dimension(unit = Dimension.DP) dp: Float) =
    TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp,
        displayMetrics
    )

fun spToPx(displayMetrics: DisplayMetrics, @Dimension(unit = Dimension.SP) sp: Float) =
    TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_SP,
        sp,
        displayMetrics
    )

fun Context.dpToPx(@Dimension(unit = Dimension.DP) dp: Float) = dpToPx(resources.displayMetrics, dp)
fun Context.spToPx(@Dimension(unit = Dimension.SP) sp: Float) = dpToPx(resources.displayMetrics, sp)

fun View.dpToPx(@Dimension(unit = Dimension.DP) dp: Float) = context.dpToPx(dp)
fun View.spToPx(@Dimension(unit = Dimension.SP) sp: Float) = context.spToPx(sp)

/**
 * Use with start <= this <= end
 *
 * Examples:
 *  (0.5f).restrict(0.1f, 0.3f) = 0.3f
 *  (0.2f).restrict(0.1f, 0.3f) = 0.2f
 *  (0.0f).restrict(0.1f, 0.2f) = 0.1f
 * */
fun <T : Comparable<T>> T.restrict(min: T, max: T): T =
    if (compareTo(min) < 0) min else if (compareTo(max) > 0) max else this


/**
 * Examples:
 *  (1.0f).fractionForRange(0f, 2f) = 0.5f
 *  (0.5f).fractionForRange(0f, 2f) = 0.25f
 * */
fun Float.fractionForRange(start: Float, end: Float): Float =
    (this - start) / (end - start)

fun Float.fractionForRange(start: Int, end: Int) =
    fractionForRange(start.toFloat(), end.toFloat())

fun Int.fractionForRange(start: Int, end: Int): Float =
    toFloat().fractionForRange(start.toFloat(), end.toFloat())

/**
 *
 * Examples:
 *  (0.5f).fractionToPositionInRange(0f, 10f) = 5.0f
 *  (0.5f).fractionToPositionInRange(5f, 15f) = 10f
 * */
fun Float.fractionToPositionInRange(start: Float, end: Float): Float =
    ((end - start) * this) + start

fun Float.fractionToPositionInRange(start: Int, end: Int): Float =
    fractionToPositionInRange(start.toFloat(), end.toFloat())