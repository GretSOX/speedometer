package com.example.myapplication

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

class DoubleTouchViewPager @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : ViewPager(context, attrs) {

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        return (if (ev.action == MotionEvent.ACTION_MOVE && ev.pointerCount != 2) false else super.onInterceptTouchEvent(
            ev
        ))
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        return if (ev.action == MotionEvent.ACTION_MOVE && ev.pointerCount != 2) false else super.onTouchEvent(
            ev
        )
    }
}