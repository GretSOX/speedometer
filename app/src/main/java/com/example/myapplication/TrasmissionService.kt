package com.example.myapplication

import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Interpolator

class TrasmissionService : Service() {

    private var listener: ITransmissionListener? = null

    private val service = object : ITransmissionService.Stub() {
        override fun subscribeToSpeed(listener: ITransmissionListener) {
            this@TrasmissionService.listener = listener
        }
    }

    private val speedGeneratorHandler =
        TransmissionDataGeneratorHandler { rpm, speed ->
            listener?.updateRpm(rpm)
            listener?.updateSpeed(speed)
        }

    override fun onCreate() {
        super.onCreate()
        speedGeneratorHandler.startGeneration()
    }

    override fun onBind(intent: Intent): IBinder =
        service

    override fun onUnbind(intent: Intent?): Boolean {
        listener = null
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        speedGeneratorHandler.stopGeneration()
    }

    private class TransmissionDataGeneratorHandler(
        private val interpolator: Interpolator = AccelerateDecelerateInterpolator(),
        private val window: Int = 100,
        private val maxSpeed: Int = 175,
        private val maxRpm: Int = 8,
        private val transmissionGearSpeedCutoffs: IntArray =
            intArrayOf(20, 40, 70, 120, 175),
        private val transmissionGearRpmCutoffs: FloatArray =
            floatArrayOf(.25f, .45f, .65f, .75f, 1f),
        private val delay: Long = 100,
        private val valueCallback: (rpm: Float, speed: Float) -> Unit
    ) : Handler() {

        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            if (msg.what == MSG_GENERATE) {
                val increment = msg.obj as Int
                val interpolatedFraction = (increment % window)
                    .fractionForRange(0, window)
                    .let { interpolator.getInterpolation(it) }
                val speed = interpolatedFraction
                    .fractionToPositionInRange(0, maxSpeed)

                val resultSpeed =
                    if (increment / window % 2 == 0) speed
                    else maxSpeed - speed

                val gearIndex = transmissionGearSpeedCutoffs
                    .indexOfFirst { speedCutoff -> speedCutoff >= resultSpeed }

                val gearSpeedCutoff = transmissionGearSpeedCutoffs[gearIndex]
                val prevGearSpeedCutoff =
                    if (gearIndex == 0) 0
                    else transmissionGearSpeedCutoffs[gearIndex - 1]
                val gearRpmCutoff = transmissionGearRpmCutoffs[gearIndex]

                val rpm: Float = resultSpeed
                    .fractionForRange(prevGearSpeedCutoff, gearSpeedCutoff)
                    .fractionToPositionInRange(0f, maxRpm * gearRpmCutoff)

                valueCallback(rpm, resultSpeed)
                val newIncrement = if (increment == Int.MAX_VALUE) 0 else increment + 1
                sendMessageDelayed(Message.obtain(this, MSG_GENERATE, newIncrement), delay)
            }
        }

        fun startGeneration() {
            stopGeneration()
            sendMessage(Message.obtain(this, MSG_GENERATE, 0))
        }

        fun stopGeneration() {
            removeCallbacksAndMessages(null)
        }

        companion object {
            private val MSG_GENERATE = R.id.msg_generate
        }
    }
}
